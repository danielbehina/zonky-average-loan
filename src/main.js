import Vue from 'vue'
import App from './App.vue'
import axios from 'axios'

Vue.config.productionTip = false

const api = axios.create({
  baseURL: "/",
})

Vue.prototype.$http = api

new Vue({
  render: h => h(App)
}).$mount('#app');
